#!/usr/bin/env bash

# /opt/libvirt-driver/run.sh

currentDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source ${currentDir}/base.sh # Get variables from base script.

VM_IP=$(_get_vm_ip)

echo "ARGUMENTS TO THE SSH SESSION:"
echo $1
cat $1
echo "***"
echo $CLONE_URL

ssh -i /home/gitlab-runner/.vagrant.d/insecure_private_key -o StrictHostKeyChecking=no vagrant@"$VM_IP" /bin/bash < "${1}"
if [ $? -ne 0 ]; then
    # Exit using the variable, to make the build as failure in GitLab
    # CI.
    exit "$BUILD_FAILURE_EXIT_CODE"
fi
